# Pritaly

A small tool to add color/highlighting to Gitaly log files or convert them to JSON.

##### Binaries can be downloaded via [Releases](https://gitlab.com/gitlab-com/support/toolbox/pritaly/releases) OR [Repository -> Tags](https://gitlab.com/gitlab-com/support/toolbox/pritaly)

### Flags
When no flag is given `pritaly` will default to colorizing the standard Gitaly log format.

Output to terminals will automatically be piped to `less`, but this can be overridden.  Piping to `head`, etc.  will also cancel this behavior.

   * `--json / -j` - Convert Gitaly logs to JSON
   * `--no-less / -n` - Prevent output from being piped to `less` when outputting to terminal. Great for checking how long it takes to render 50,000 lines of text.

#### Color output:

![color-screenshot](/uploads/c31f1277fec931a1fc0d4b84f659e109/Screen_Shot_2019-02-10_at_11.51.53_PM.png)

#### Conversion to JSON:

![pritaly-json](/uploads/0ca18a33168a694f98db89136b95b951/json-low-fr.gif)
