# Example Uses

## Show all errors and their counts

```
pritaly $FILE -j | jq -s 'map(select(.error != null)) | group_by(.error) | sort_by(-length) | .[] | "\(length) \(.[0].error)"'

```
