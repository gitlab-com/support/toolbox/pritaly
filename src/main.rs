use atty::Stream;
use clap::{App, Arg};
use indexmap::IndexMap;
use rayon::prelude::*;
use serde_derive::Serialize;
use std::fs;
use std::io::{prelude::*, stdin, stdout, Error};
use std::process::{Child, Command, Stdio};

#[derive(Clone, Debug, PartialEq)]
struct Val<'a> {
    key: &'a str,
    value: &'a str,
}

#[derive(Clone, Debug, PartialEq)]
struct Log<'a> {
    time: &'a str,
    vals: Vec<Val<'a>>,
}

#[derive(Debug, PartialEq, Serialize)]
#[serde(untagged)]
enum JsonLog<'a> {
    Str(&'a str),
    Num(f64),
    Map(Map<'a>),
}

type Map<'a> = IndexMap<&'a str, JsonLog<'a>>;

impl<'a> From<&'a str> for JsonLog<'a> {
    fn from(s: &'a str) -> Self {
        if s.starts_with(char::is_numeric) {
            if let Ok(num) = s.parse::<f64>() {
                JsonLog::Num(num)
            } else {
                JsonLog::Str(s)
            }
        } else if s.starts_with('"') {
            JsonLog::Str(s.trim_matches('"'))
        } else {
            JsonLog::Str(s)
        }
    }
}

impl<'a> From<&Log<'a>> for JsonLog<'a> {
    fn from(log: &Log<'a>) -> Self {
        let mut map = IndexMap::new();
        map.insert("log_time", JsonLog::from(log.time));

        for val in &log.vals {
            let mut keys = val.key.split('.').peekable();
            let first_key = keys.next().expect("Invalid split");

            if let None = keys.peek() {
                map.insert(first_key, JsonLog::from(val.value));
            } else {
                let mut entry = map.entry(first_key).or_insert(JsonLog::Map(Map::new()));

                while let Some(next_key) = keys.next() {
                    if let Some(_) = keys.peek() {
                        if let JsonLog::Map(sub_map) = entry {
                            entry = sub_map.entry(next_key).or_insert(JsonLog::Map(Map::new()));
                        }
                    } else if let JsonLog::Map(sub_map) = entry {
                        sub_map.insert(next_key, JsonLog::from(val.value));
                    }
                }
            }
        }
        JsonLog::Map(map)
    }
}

#[derive(Clone, Copy, Debug)]
enum PrintMode {
    Json,
    Color,
}

fn main() {
    let args = App::new("Pritaly")
        .version("0.2.0")
        .author("Will Chandler <wchandler@gitlab.com>")
        .about("Small tool to highlight Gitaly logs and convert them to JSON")
        .arg(
            Arg::with_name("INPUT")
                .help("File to be processed")
                .required(false)
                .takes_value(true)
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("json")
                .short("j")
                .long("json")
                .help("Convert log to JSON")
                .conflicts_with("color"),
        )
        .arg(
            Arg::with_name("no_less")
                .short("n")
                .long("no-less")
                .help("Do not pipe output to 'less'"),
        )
        .get_matches();

    let input = match args.value_of("INPUT") {
        Some(file_name) => read_file(file_name),
        None => read_stdin(),
    };

    let logs: Vec<_> = input
        .par_lines()
        .filter_map(|line| parse_line(line))
        .collect();

    let print_mode = if args.is_present("json") {
        PrintMode::Json
    } else {
        PrintMode::Color
    };

    if atty::is(Stream::Stdout) && !args.is_present("no_less") {
        if let Err(e) = print_to_less(print_mode, &logs) {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    } else {
        print_to_stdout(print_mode, &logs);
    }
}

fn read_stdin() -> String {
    let mut stdin = stdin();
    let mut buffer = String::new();

    match stdin.read_to_string(&mut buffer) {
        Ok(_) => buffer,
        Err(e) => {
            eprintln!("Failed to read stdin: {}", e);
            std::process::exit(1)
        }
    }
}

fn read_file(file_name: &str) -> String {
    match fs::read_to_string(&file_name) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{}: {}", file_name, e);
            std::process::exit(1)
        }
    }
}

fn parse_line(line: &str) -> Option<Log> {
    let mut initial_split = line.splitn(2, ' ');
    let time = initial_split.next()?;

    let mut vals = Vec::new();
    let log_body = initial_split.next()?;

    let mut remainder = log_body.splitn(2, '=');
    let raw_key_tok = remainder.next()?;

    // Take only the str after the last space in the first key
    // K8s instances will place the server name after time, which
    // we want to ignore
    let mut raw_key = raw_key_tok.rsplitn(2, ' ').next()?;

    while let Some(rem) = remainder.next() {
        let key = raw_key.trim_start();

        if rem.starts_with('"') {
            let val_end = rem.find("\" ")?;
            let (value, new_rem) = rem.split_at(val_end + 1);

            vals.push(Val { key, value });
            remainder = new_rem.splitn(2, '=');
        } else if let Some(val_end) = rem.find(' ') {
            let (value, new_rem) = rem.split_at(val_end);

            vals.push(Val { key, value });
            remainder = new_rem.splitn(2, '=');
        } else {
            vals.push(Val { key, value: rem }); // End of line
        }

        raw_key = remainder.next().unwrap_or_default();
    }

    Some(Log { time, vals })
}

fn print_to_less(print_mode: PrintMode, logs: &[Log]) -> Result<(), Error> {
    let mut less = launch_less()?;
    let less_handle = less.stdin.as_mut().ok_or_else(Error::last_os_error)?;

    print_logs(less_handle, print_mode, &logs);

    less.wait()?;
    Ok(())
}

fn launch_less() -> Result<Child, Error> {
    Command::new("less")
        .arg("--RAW-CONTROL-CHARS")
        .stdin(Stdio::piped())
        .spawn()
}

fn print_to_stdout(print_mode: PrintMode, logs: &[Log]) {
    let stdout = stdout();
    let stdout_handle = stdout.lock();

    print_logs(stdout_handle, print_mode, &logs);
}

fn print_logs(mut handle: impl Write, print_mode: PrintMode, logs: &[Log]) {
    // Ignore result as we expect failures when piping to head
    let _ = match print_mode {
        PrintMode::Json => write_json(logs, &mut handle),
        PrintMode::Color => write_color(logs, &mut handle),
    };
}

fn write_json(logs: &[Log], out: &mut impl Write) -> Result<(), Error> {
    let json_logs: Vec<_> = logs
        .par_iter()
        .map(JsonLog::from)
        .filter_map(|jlog| serde_json::to_string(&jlog).ok())
        .collect();

    for log in json_logs {
        writeln!(out, "{}", log)?;
    }
    Ok(())
}

fn write_color(logs: &[Log], out: &mut impl Write) -> Result<(), Error> {
    let color_logs: Vec<_> = logs
        .par_iter()
        .map(|log| {
            let values_colored = log.vals.iter().fold(String::with_capacity(700), |s, val| {
                s + " " + GREEN + val.key + RESET + GRAY + "=" + RESET + WHITE + val.value + RESET
            });
            format!(
                "{time_color}{time}{reset}{values}",
                time_color = RED,
                time = log.time,
                reset = RESET,
                values = values_colored
            )
        })
        .collect();

    for log in color_logs {
        writeln!(out, "{}", log)?;
    }
    Ok(())
}

const RED: &str = "\x1b[31;1;m";
const GREEN: &str = "\x1b[32m";
const WHITE: &str = "\x1b[37m";
const GRAY: &str = "\x1b[37;2;m";
const RESET: &str = "\x1b[0m";

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_standard_completed() {
        let input = r##"2019-02-18_16:40:26.42381 time="2019-02-18T16:40:26Z" level=info msg="finished unary call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=FindCommit grpc.request.deadline="2019-02-18T16:40:47Z" grpc.request.fullMethod=/gitaly.CommitService/FindCommit grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.CommitService grpc.start_time="2019-02-18T16:40:17Z" grpc.time_ms=8978.278 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input);
        assert!(output.is_some());
    }

    #[test]
    fn parse_standard_time() {
        let input = r##"2019-02-18_16:40:26.42381 time="2019-02-18T16:40:26Z" level=info msg="finished unary call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=FindCommit grpc.request.deadline="2019-02-18T16:40:47Z" grpc.request.fullMethod=/gitaly.CommitService/FindCommit grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.CommitService grpc.start_time="2019-02-18T16:40:17Z" grpc.time_ms=8978.278 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input).unwrap();
        assert_eq!("grpc", output.vals.last().unwrap().value);
    }

    #[test]
    fn parse_standard_value() {
        let input = r##"2019-02-18_16:40:26.42381 time="2019-02-18T16:40:26Z" level=info msg="finished unary call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=FindCommit grpc.request.deadline="2019-02-18T16:40:47Z" grpc.request.fullMethod=/gitaly.CommitService/FindCommit grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.CommitService grpc.start_time="2019-02-18T16:40:17Z" grpc.time_ms=8978.278 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input).unwrap();
        assert_eq!("2019-02-18_16:40:26.42381", output.time);
    }

    #[test]
    fn parse_healthcheck_completed() {
        let input = r##"2019-02-18_16:40:32.76183 time="2019-02-18T16:40:32Z" level=warning msg="health check failed" error="rpc error: code = DeadlineExceeded desc = Deadline Exceeded" worker.name=gitaly-ruby.1"##;
        let output = parse_line(input);
        assert!(output.is_some());
    }

    #[test]
    fn parse_healthcheck_value() {
        let input = r##"2019-02-18_16:40:32.76183 time="2019-02-18T16:40:32Z" level=warning msg="health check failed" error="rpc error: code = DeadlineExceeded desc = Deadline Exceeded" worker.name=gitaly-ruby.1"##;
        let output = parse_line(input).unwrap();

        assert_eq!(
            "\"rpc error: code = DeadlineExceeded desc = Deadline Exceeded\"",
            output.vals[3].value
        );
    }

    #[test]
    fn parse_hostname_completed() {
        let input = r##"2019-02-20_20:22:08.64248 p-aaaa-git-001 gitaly: time="2019-02-20T20:22:08Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.method=SSHUploadPack grpc.request.fullMethod=/gitaly.SSHService/SSHUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SSHService grpc.start_time="2019-02-20T20:22:08Z" grpc.time_ms=3.494 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input);

        assert!(output.is_some());
    }

    #[test]
    fn parse_hostname_log_time() {
        let input = r##"2019-02-20_20:22:08.64248 p-aaaa-git-001 gitaly: time="2019-02-20T20:22:08Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.method=SSHUploadPack grpc.request.fullMethod=/gitaly.SSHService/SSHUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SSHService grpc.start_time="2019-02-20T20:22:08Z" grpc.time_ms=3.494 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input).unwrap();

        assert_eq!("2019-02-20_20:22:08.64248", output.time);
    }

    #[test]
    fn parse_hostname_time_value() {
        let input = r##"2019-02-20_20:22:08.64248 p-aaaa-git-001 gitaly: time="2019-02-20T20:22:08Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.method=SSHUploadPack grpc.request.fullMethod=/gitaly.SSHService/SSHUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SSHService grpc.start_time="2019-02-20T20:22:08Z" grpc.time_ms=3.494 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input).unwrap();

        assert_eq!("\"2019-02-20T20:22:08Z\"", output.vals[0].value);
    }

    #[test]
    fn parse_hostname_strip_hostname() {
        let input = r##"2019-02-20_20:22:08.64248 p-aaaa-git-001 gitaly: time="2019-02-20T20:22:08Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.method=SSHUploadPack grpc.request.fullMethod=/gitaly.SSHService/SSHUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SSHService grpc.start_time="2019-02-20T20:22:08Z" grpc.time_ms=3.494 peer.address=@ span.kind=server system=grpc"##;
        let output = parse_line(input).unwrap();
        let key_vals = output
            .vals
            .iter()
            .fold(String::new(), |s, val| s + val.key + val.value);

        assert_eq!(false, key_vals.contains("p-aaaa-git-001 gitaly:"));
    }
}
